<?php

return array(//TABLEAU DES PARAMETRES
    'router' => array(//ROUTAGE
        'routes' => array(//ensemble de routes
            'rContact' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'CtrlIndex',
                        'action' => 'index',
                    ),
                ),
            ),
            'rContact-Gestion' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/:action[/:id]',
                    'defaults' => array(
                        'controller' => 'CtrlIndex',
                        'constraints' => array(
                            'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                        ),
                    ),
                ),
            ),
        ),
    ), //FIN ROUTAGE
    'controllers' => array(//CONTROLLER
        'invokables' => array(//déclaration des controleurs dynamiquement initialisables
            'CtrlIndex' => 'Contact\Controller\IndexController'
        ),
    ), //FIN CONTROLLER
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'contact/index/index' => __DIR__ . '/../view/contact/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
); //FIN TABLEAU PARAMETRES


